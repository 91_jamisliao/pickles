﻿param (
   [string]$solutionPath = "C:\MyProject\",
   [string]$testToolPath = "C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\IDE\MsTest.exe"
)

#$testResultFolder="$($solutionPath)\TestResults"
$testResultPath="$($solutionPath)TestResults\TestResult.trx"

$featurePath=$solutionPath

$pickleToolPath = (Get-ChildItem $($solutionPath) -Recurse -Filter "pickles.exe" | select-object FullName).FullName
$pickleResultPath="$($solutionPath)PicklesResults"

If (Test-Path $testResultPath){
	Remove-Item $testResultPath
}

$testDll = Get-ChildItem $solutionPath -Recurse -Filter "*Test.dll" | Where-Object { ($_.FullName -notmatch "obj") -and ($_.FullName -notmatch "TestResults") } | Select-Object FullName
$testContainerParameter=""
foreach($dll in $testDll){
    $testContainerParameter += "/testcontainer:$($dll.FullName) "
}

$cmd = """$testToolPath"" $testContainerParameter /resultsfile:$testResultPath"
Write-Host $cmd
iex "& $cmd"

$cmd = "$pickleToolPath run --feature-directory=$featurePath --output-directory=$pickleResultPath --documentation-format=dhtml --test-results-format=mstest --link-results-file=$testResultPath"
iex "& $cmd"

Invoke-Item $pickleResultPath\index.html
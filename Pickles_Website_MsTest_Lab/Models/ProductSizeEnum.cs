﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pickles_Website_MsTest_Lab.Models
{
    public enum ProductSizeEnum
    {
        Big = 0,
        Medium = 1,
        Small = 2
    }
}
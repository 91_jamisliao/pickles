﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pickles_Website_MsTest_Lab.Models
{
    public enum ProductTypeEnum
    {
        Book,
        Electronic,
        Desktop,
        Laptop
    }
}